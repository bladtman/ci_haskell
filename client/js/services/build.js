'use strict';

angular.module('ttCI')
    .service('Build', function Build($http) {

        this.post = function(quiz){
            var url = Config.url+'/';

            return new $http.post(url,{'data':'data'});
        }

        this.get = function(answerData){
            var url = Config.url+'/';

            return new $http.get(url)
        }
    });
