import Text.Printf
import Text.Regex
import Text.JSON
import Data.Maybe
import Data.List
import Control.Applicative
import System.Process
import Network
import Control.Concurrent
import System.IO

main = withSocketsDo $ do
    sock <- listenOn $ PortNumber 5002
    loop sock

loop sock = do
    (h,_,_) <- accept sock
    forkIO $ body h
    loop sock
   where
    body h = do
        (_, stdOut, _) <- readProcessWithExitCode "rake" ["test", "2>&1"] ""
        putStrLn stdOut
        hPutStr h $ msg $ toJson stdOut
        hFlush h
        hClose h

msg :: String -> String
msg str = printf "HTTP/1.0 200 OK\r\nContent-Length: %d\r\n\r\n%s\r\n" (length str) str

matchError str =
  isJust $ matchRegex (mkRegex ".*) Error\n\n") str

splitError str =
  splitRegex (mkRegexWithOpts "^.*) (Error|Failure).*$" True True) str

toJson str =
  encode $ splitError str
